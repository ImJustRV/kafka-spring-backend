package ru.meme.springkafka.kafkademo

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.kafka.support.serializer.JsonSerializer

val mapper: ObjectMapper = ObjectMapper().registerModules(Jdk8Module(), JavaTimeModule())

class DemoSerializer: JsonSerializer<Any>(mapper)
class DemoDeserializer: JsonDeserializer<Any>(mapper)