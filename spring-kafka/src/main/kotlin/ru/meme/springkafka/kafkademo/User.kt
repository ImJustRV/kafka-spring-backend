package ru.meme.springkafka.kafkademo

import java.time.LocalDateTime

data class User(
    val firstName: String,
    val age: Int,
    val createdAt: LocalDateTime = LocalDateTime.now()
)